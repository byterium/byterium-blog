---
title: 'How I Built This Blog'
author: 'Joe Kriefall'
date: 2022-01-19
---

As I transition from web development to data engineering, I figured I would document what I learn along the way in a blog. Coming from the web development world, my first inclination wasn’t to start up a WordPress account. Rather, it was to look into how hard it would be to build my own framework. I wanted my blog to deploy to simple to build, use and deploy, so the conventional three tiered architecture that was all the rage when I started my career – a UI, an API, and a backing database – was out of the question. Deploying static files – HTML, CSS, and maybe some JavaScript – is a lot easier and cheaper than hosting a database and a web server for an API. In fact, at the time of writing there are numerous ways to host static websites for free, including GitLab Pages, which I picked for this project. Hosting on GitLab has the added advantage of an easy-to-use pipeline for building and deploying, as well as hosting the source code. 

What I essentially wanted to do was to build my own static site generator, like Jekyll, so my first step was to search the internet for “build your own static site generator”. The first result that caught my eye was [Build your own static site generator (because why not?)](https://izolate.net/posts/build-your-own-static-site-generator), so my static site generator is largely based on that author’s post.

Templating is done with Nunjucks:
```javascript
function buildHtmlDocFromTemplate(templateFileName, outputFileName, data) {
  const template = readFileSync(path.join(templateDir, templateFileName), 'utf-8')
  const htmlDoc = nunjucksEnv.renderString(template, data)
  const outputPath = path.join(outputDir, outputFileName)
  writeFileSync(outputPath, htmlDoc, 'utf-8')
}
```