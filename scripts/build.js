import { program } from 'commander'
import frontMatter from 'front-matter'
import { copyFileSync, readdirSync, readFileSync, writeFileSync } from 'fs'
import { emptyDirSync, ensureDirSync } from 'fs-extra'
import nunjucks from 'nunjucks'
import path from 'path'
import remarkParse from 'remark-parse'
import remarkRehype from 'remark-rehype'
import rehypeHighlight from 'rehype-highlight'
import rehypeStringify from 'rehype-stringify'
import { unified } from 'unified'
import remarkStringify from 'remark-stringify'

const { baseUrl } = program
  .option('--base-url <url>', 'base url', 'http://localhost:8080')
  .parse()
  .opts()

const outputDir = 'public'
const postDir = 'posts'
const assetDir = 'assets'
const templateDir = 'templates'

const nunjucksEnv = new nunjucks.Environment(
  new nunjucks.FileSystemLoader('templates')
)
nunjucksEnv.addGlobal('year', new Date().getFullYear())
nunjucksEnv.addGlobal('baseUrl', baseUrl)

/** Converts Markdown to HTML snippet with syntax highlighting */
function convertMarkdownToHtml(markdown) {
  return unified()
    .use(remarkParse)
    .use(remarkRehype)
    .use(rehypeHighlight)
    .use(rehypeStringify)
    .processSync(markdown)
}

/** Get first paragraph of post */
function extractFirstParagraph(markdown) {
  return unified()
    .use(remarkParse)
    .use(() => tree => {
      tree.children.splice(1)
    })
    .use(remarkStringify)
    .processSync(markdown)
}

/** Load post & convert Markdown to HTML */
function loadPost(sourcePath) {
  const markdownFile = readFileSync(sourcePath, 'utf-8')

  const { body: contentMarkdown, attributes } = frontMatter(markdownFile)
  const contentHtml = convertMarkdownToHtml(contentMarkdown)
  const { name: slug } = path.parse(sourcePath)
  const preview = extractFirstParagraph(contentMarkdown)

  return {
    contentMarkdown,
    contentHtml,
    slug,
    preview,
    ...attributes
  }
}

/** Load all posts */
function loadAllPosts() {
  return readdirSync(postDir)
    .map(post => loadPost(path.join(postDir, post)))
    .sort(post => -post.date)
}

/** Compile & save HTML document from template */
function buildHtmlDocFromTemplate(templateFileName, outputFileName, data) {
  const template = readFileSync(path.join(templateDir, templateFileName), 'utf-8')
  const htmlDoc = nunjucksEnv.renderString(template, data)
  const outputPath = path.join(outputDir, outputFileName)
  writeFileSync(outputPath, htmlDoc, 'utf-8')
}

/** Build & save post pages */
function buildPostPages(posts) {
  posts.forEach(post => {
    buildHtmlDocFromTemplate('post.njk', `${post.slug}.html`, post)
  })
}

/** Build & save home page */
function buildHomePage(posts) {
  buildHtmlDocFromTemplate('home.njk', 'index.html', { posts })
}

 /** Build & save archive page */
function buildArchivePage(posts) {
  buildHtmlDocFromTemplate('archive.njk', 'archive.html', { posts })
}

/** Clear output directory */
function resetOutputDir() {
  emptyDirSync(outputDir)
  ensureDirSync(path.join(outputDir, 'css'))
  ensureDirSync(path.join(outputDir, 'fonts'))
}

/** Copy assets (icons, fonts, css) to output directory */
function copyAssetsToOutputDir() {
  /* copy favicon */
  copyFileSync(
    path.join(assetDir, 'favicon.ico'), 
    path.join(outputDir, 'favicon.ico')
  )

  /* copy fonts */
  copyFileSync(
    'node_modules/@fontsource/source-serif-4/files/source-serif-4-latin-400-normal.woff2',
    path.join(outputDir, 'fonts/source-serif-4-latin-400-normal.woff2')
  )
  copyFileSync(
    'node_modules/@fontsource/source-serif-4/files/source-serif-4-latin-400-normal.woff',
    path.join(outputDir, 'fonts/source-serif-4-latin-400-normal.woff')
  )
  copyFileSync(
    'node_modules/@fontsource/source-sans-3/files/source-sans-3-latin-400-normal.woff2',
    path.join(outputDir, 'fonts/source-sans-3-latin-400-normal.woff2')
  )
  copyFileSync(
    'node_modules/@fontsource/source-sans-3/files/source-sans-3-latin-400-normal.woff',
    path.join(outputDir, 'fonts/source-sans-3-latin-400-normal.woff')
  )
  copyFileSync(
    'node_modules/@fontsource/source-code-pro/files/source-code-pro-latin-400-normal.woff2',
    path.join(outputDir, 'fonts/source-code-pro-latin-400-normal.woff2')
  )
  copyFileSync(
    'node_modules/@fontsource/source-code-pro/files/source-code-pro-latin-400-normal.woff',
    path.join(outputDir, 'fonts/source-code-pro-latin-400-normal.woff')
  )

  /* copy css files */
  copyFileSync(
    'node_modules/modern-normalize/modern-normalize.css',
    path.join(outputDir, 'css/modern-normalize.css')
  )
  copyFileSync(
    'node_modules/highlight.js/styles/a11y-light.css',
    path.join(outputDir, 'css/highlightjs.css')
  )
  copyFileSync(
    path.join(assetDir, 'css/main.css'),
    path.join(outputDir, 'css/main.css')
  )
}

resetOutputDir()
copyAssetsToOutputDir()
const posts = loadAllPosts()
buildHomePage(posts)
buildPostPages(posts)
buildArchivePage(posts)
