import { readdirSync } from 'fs'

const posts = readdirSync('posts')
  .flatMap(category => readdirSync(`posts/${category}`)
    .map(post => ({ post, category }))
  )
  
console.log(posts)
